package com.example.bootsample.web.controller;

import com.example.bootsample.service.TodoService;
import com.example.bootsample.web.form.TodoForm;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/")
public class TodoController {

    private TodoService todoService;

    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping
    public String root(Model model) {
        model.addAttribute("todoList", todoService.findAll());
        model.addAttribute("todoForm", new TodoForm());
        return "index";
    }

    @PostMapping("/add")
    public String add(@Validated TodoForm todoForm, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return redirectToIndex();
        }
        todoService.add(todoForm.getText());
        return redirectToIndex();
    }

    @PostMapping("/done/{id}")
    public String done(@PathVariable("id") long id) {
        todoService.done(id);
        return redirectToIndex();
    }

    @PostMapping("/remove/{id}")
    public String remove(@PathVariable("id") long id) {
        todoService.remove(id);
        return redirectToIndex();
    }

    private String redirectToIndex() {
        return "redirect:/";
    }

}

