package com.example.bootsample.entity;

public class Todo {
    private long id;

    private String text;

    private boolean done;

    public Todo(long id, String text) {
        this.id = id;
        this.text = text;
        done = false;
    }

    public Todo(long id, String text, boolean done) {
        this.id = id;
        this.text = text;
        this.done = done;
    }

    public String getText() {
        return text;
    }

    public long getId() {
        return id;
    }

    public boolean isDone() {
        return done;
    }

    public void done() {
        this.done = true;
    }
}
